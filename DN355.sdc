#**************************************************************
# This .sdc file is created by Terasic Tool.
# Users are recommended to modify this file to match users logic.
#**************************************************************

#**************************************************************
# Create Clock
#**************************************************************
#create_clock -period "10.0 MHz" [get_ports ADC_CLK_10]
create_clock -period "50.0 MHz" [get_ports CLOCK_50]
#create_clock -period "50.0 MHz" [get_ports CLOCK2_50]

#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************
set_false_path -from {KEY[0]}
set_false_path -from {KEY[1]}
set_false_path -from {SW[0]}
set_false_path -from {SW[1]}
set_false_path -from {SW[2]}
set_false_path -from {SW[3]}
set_false_path -from {SW[4]}
set_false_path -from {SW[5]}
set_false_path -from {SW[6]}
set_false_path -from {SW[7]}
set_false_path -from {SW[8]}
set_false_path -from {SW[9]}

set_false_path -to {LEDR[0]}
set_false_path -to {LEDR[1]}
set_false_path -to {LEDR[2]}
set_false_path -to {LEDR[3]}
set_false_path -to {LEDR[4]}
set_false_path -to {LEDR[5]}
set_false_path -to {LEDR[6]}
set_false_path -to {LEDR[7]}
set_false_path -to {LEDR[8]}
set_false_path -to {LEDR[9]}

set_false_path -to {HEX0[0]}
set_false_path -to {HEX0[1]}
set_false_path -to {HEX0[2]}
set_false_path -to {HEX0[3]}
set_false_path -to {HEX0[4]}
set_false_path -to {HEX0[5]}
set_false_path -to {HEX0[6]}
set_false_path -to {HEX0[7]}

set_false_path -to {HEX1[0]}
set_false_path -to {HEX1[1]}
set_false_path -to {HEX1[2]}
set_false_path -to {HEX1[3]}
set_false_path -to {HEX1[4]}
set_false_path -to {HEX1[5]}
set_false_path -to {HEX1[6]}
set_false_path -to {HEX1[7]}

set_false_path -to {HEX2[0]}
set_false_path -to {HEX2[1]}
set_false_path -to {HEX2[2]}
set_false_path -to {HEX2[3]}
set_false_path -to {HEX2[4]}
set_false_path -to {HEX2[5]}
set_false_path -to {HEX2[6]}
set_false_path -to {HEX2[7]}

set_false_path -to {HEX3[0]}
set_false_path -to {HEX3[1]}
set_false_path -to {HEX3[2]}
set_false_path -to {HEX3[3]}
set_false_path -to {HEX3[4]}
set_false_path -to {HEX3[5]}
set_false_path -to {HEX3[6]}
set_false_path -to {HEX3[7]}

set_false_path -to {HEX4[0]}
set_false_path -to {HEX4[1]}
set_false_path -to {HEX4[2]}
set_false_path -to {HEX4[3]}
set_false_path -to {HEX4[4]}
set_false_path -to {HEX4[5]}
set_false_path -to {HEX4[6]}
set_false_path -to {HEX4[7]}

set_false_path -to {HEX5[0]}
set_false_path -to {HEX5[1]}
set_false_path -to {HEX5[2]}
set_false_path -to {HEX5[3]}
set_false_path -to {HEX5[4]}
set_false_path -to {HEX5[5]}
set_false_path -to {HEX5[6]}
set_false_path -to {HEX5[7]}


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************



#**************************************************************
# Set Load
#**************************************************************



