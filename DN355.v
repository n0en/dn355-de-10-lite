module DN355(SW, KEY, CLOCK_50, LEDR, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, GPIO);
	
   input  [9:0] SW;
   input  [1:0] KEY;
   input CLOCK_50;
   output [9:0] LEDR;
   output [7:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	inout [7:0] GPIO;
	
	wire sysReset;
	wire runIndicator;
	
	wire [17:0] diagnosticCode;
   
	wire [14:0] currentIC;
	wire [2:0] cpuState;
	wire [17:0] currentIR;
	wire [17:0] regA;
	wire [17:0] regQ;
	wire [17:0] regX1;
	wire [17:0] regX2;
	wire [17:0] regX3;
	wire [5:0] regS;

	wire systemClock;
  	wire clock4Hz;

   function automatic [6:0] digit;
      input [2:0] num; 
      case (num)
         0:  digit = 7'b1000000;  // 0
         1:  digit = 7'b1111001;  // 1
         2:  digit = 7'b0100100;  // 2
         3:  digit = 7'b0110000;  // 3
         4:  digit = 7'b0011001;  // 4
         5:  digit = 7'b0010010;  // 5
         6:  digit = 7'b0000010;  // 6
         7:  digit = 7'b1111000;  // 7
      endcase
   endfunction

	// Memory definitions
	wire [14:0] memoryAddress;
	wire [17:0] memoryWriteData;
	wire [17:0] memoryReadData;
	wire memoryChipSelect;
	wire memoryWriteEnable;
	wire resetButton;
	wire runSwitch;
	wire programSwitch;			// When high the PGM module will be used for init, when low the ROM will be used for init
	wire singleStepButton;
	wire clock1Hz;
	wire slowSwitch;
	wire cpuClock;
	wire [3:0] displayMode;
	wire [7:0] indicators;
	wire overflowFlag;
	wire zeroFlag;
	wire negativeFlag;
	wire carryFlag;
	wire [14:0] effectiveAddress;
	
	assign zeroFlag = indicators[0];
	assign negativeFlag = indicators[1];
	assign carryFlag = indicators[2];
	assign overflowFlag = indicators[3];

	
	wire [1:0] programState;
	wire [3:0] memoryState;
	
	wire [17:0] fullState;
	
	assign fullState[2:0] = cpuState;
	assign fullState[5:3] = {1'b0, programState};
	assign fullState[8:6] = {1'b0, fetchState};
	assign fullState[11:9] = {1'b0, resolveState};
	assign fullState[14:12] = memoryState[2:0];
	assign fullState[17:15] = {2'b00, memoryState[3]};
	
	wire [17:0] indicatorsAndSreg;
	
	assign indicatorsAndSreg = {zeroFlag, negativeFlag, carryFlag, overflowFlag, 1'b0, 1'b0, 1'b0, 1'b0, 3'b00, regS};

	
	// The display mode selection and definition
	assign displayMode = SW[8:5];
	
	parameter DISPLAY_MODE_IR 		= 4'b0000;
	parameter DISPLAY_MODE_A  		= 4'b0001;
	parameter DISPLAY_MODE_Q  		= 4'b0010;
	parameter DISPLAY_MODE_X1 		= 4'b0011;
	parameter DISPLAY_MODE_X2 		= 4'b0100;
	parameter DISPLAY_MODE_X3 		= 4'b0101;
	parameter DISPLAY_MODE_IC 		= 4'b0111;
	parameter DISPLAY_MODE_DIAG	= 4'b1000;
	parameter DISPLAY_MODE_EA		= 4'b1001;
	parameter DISPLAY_MODE_STATE	= 4'b1010;
	parameter DISPLAY_MODE_IND		= 4'b1011;
	
	wire [17:0] displaySource;
	
	assign displaySource = (displayMode == DISPLAY_MODE_IR) ? currentIR :
								(displayMode == DISPLAY_MODE_A) ? regA : 
								(displayMode == DISPLAY_MODE_Q) ? regQ : 
								(displayMode == DISPLAY_MODE_X1) ? regX1 : 
								(displayMode == DISPLAY_MODE_X2) ? regX2 : 
								(displayMode == DISPLAY_MODE_X3) ? regX3 : 
								(displayMode == DISPLAY_MODE_IC) ? currentIC :
								(displayMode == DISPLAY_MODE_DIAG) ? diagnosticCode :
								(displayMode == DISPLAY_MODE_EA) ? effectiveAddress :
								(displayMode == DISPLAY_MODE_STATE) ? fullState :
								(displayMode == DISPLAY_MODE_IND) ? indicatorsAndSreg :
								18'b0;
								
	assign HEX0[6:0] = digit(displaySource[2:0]);
	assign HEX0[7]   = ~memoryChipSelect;
	assign HEX1[6:0] = digit(displaySource[5:3]);
	assign HEX1[7]   = ~memoryWriteEnable;
	assign HEX2[6:0] = digit(displaySource[8:6]);
	assign HEX2[7]   = ~overflowFlag;
	assign HEX3[6:0] = digit(displaySource[11:9]);
	assign HEX3[7]   = ~zeroFlag;
	assign HEX4[6:0] = digit(displaySource[14:12]);
	assign HEX4[7]   = ~negativeFlag;
	assign HEX5[6:0] = digit(displaySource[17:15]);
	assign HEX5[7]   = ~carryFlag;
	

	assign LEDR[8] = runIndicator;
	assign LEDR[7:5] = cpuState;
	assign LEDR[4:0] = memoryAddress[4:0]; //currentIC[4:0];
	
	assign systemClock = CLOCK_50;
	
	
	assign resetButton = KEY[0];
	assign singleStepButton = !KEY[1];
	
	// Establish several clocks
	Clock4Hz _clock4Hz0(systemClock, clock4Hz);			// 4 Hz clock for debouncing
	ClockDivideBy4 _clock1Hz0(clock4Hz, clock1Hz);		// 1 Hz clock for running the CPU very slowly

	assign LEDR[9] = clock1Hz;

	
	// Debounce the run switch
	SwitchDebouncer _debouncer0(SW[0], clock4Hz, runSwitch);
	SwitchDebouncer _debouncer1(SW[1], clock4Hz, programSwitch);
	SwitchDebouncer _debouncer9(SW[9], clock4Hz, slowSwitch);
	
	assign cpuClock = (slowSwitch) ? clock1Hz : systemClock;
	
	// Set up the reset circuit to give a timed reset pulse
	ResetTimer _reset0(systemClock, resetButton, sysReset);
	
	// Define the block RAM to use as main storage
   SingleAddressSeparateReadWriteRam _ram0(cpuClock, memoryChipSelect, memoryWriteEnable, memoryAddress, memoryWriteData, memoryReadData);
	
	// Define the J1 CPU
	wire j1LED;
	wire uartRx;
	wire uartTx;
	wire uartCts;
	wire uartRts;
	
//	assign uartCts = GPIO[1];
//	assign uartRts = GPIO[3];
	assign uartRxD = GPIO[5];
	assign uartTxD = GPIO[7];
	
	J1Module _j1module(systemClock, j1LED, SW[2], uartRx, uartTx, sysReset);
	
	// Pull it all together with the CPU
	Datanet355Cpu _cpu0(cpuClock, sysReset, runSwitch, singleStepButton, programSwitch,
				memoryReadData, memoryAddress, memoryWriteData, memoryChipSelect, memoryWriteEnable, 
				runIndicator, currentIC, cpuState, fetchState, resolveState, memoryState,
				currentIR, regA, regQ, regX1, regX2, regX3, regS, indicators,
				programState,
				effectiveAddress, diagnosticCode
				);
   
endmodule
