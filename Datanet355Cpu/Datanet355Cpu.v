module Datanet355Cpu (
	input wire clk,							// System clock
	input wire reset,							// System reset signal
	input wire run,							// Signal to CPU to run the instruction cycle
	input wire singleStepRequest,			// Signal to CPU to single step
	input wire programRequest,				// Signal to use PGM for initial load (when low use ROM)
	input wire [17:0] memoryReadData,	// Data read from memory on read cycle
	output reg [14:0] memoryAddress, 	// The memory address the CPU wants to read or write
	output reg [17:0] memoryWriteData, 	// Data to write on memory write cycle
	output reg memoryChipSelect, 			// Chip select signal for selecting the RAM module
	output reg memoryWriteEnable, 		// Write / not Read signal for the memory cycle
	output reg runIndicator,				// Signal that shows CPU is currently running
	output reg [14:0] currentIC,			// The current value of the instruction counter
	output reg [2:0] cpuState,			// Current CPU state
	output reg [1:0] fetchState,			// Current FETCH cycle state
	output reg [1:0] resolveState,		// Current RESOLVE cycle state
	output reg [3:0] memoryState,			// Current MEMORY cycle state
	output reg [17:0] instruction,		// The current instruction register being decoded
	output reg [17:0] regA,					// The current value in the A register
	output reg [17:0] regQ,					// The current value in the Q register
	output reg [17:0] regX1,				// The current value in the X1 register
	output reg [17:0] regX2,				// The current value in the X2 register
	output reg [17:0] regX3,				// The current value in the X3 register
	output reg [5:0] regS,					// The current value in the S register
	output wire [7:0] indicators,			// The current value of the various indicator flags
	output reg [1:0] programState,
	output reg [14:0] effectiveAddress,	// The calculated effective address for a memory reference instruction
	output reg [17:0] diagnosticCode		// A code indicating where a failure to CPU_DECODE an opcode happened
);
	
	// Internal CPU State Machine storage
	parameter CPU_FETCH     = 3'b000;
	parameter CPU_DECODE    = 3'b001;
	parameter CPU_RESOLVE   = 3'b010;
	parameter CPU_EXECUTE   = 3'b011;
	parameter CPU_MEMORY	  	= 3'b100;
	parameter CPU_FINISH    = 3'b101;
	parameter CPU_PROG_LOAD	= 3'b110;
	parameter CPU_HALT      = 3'b111;
	
	reg [14:0] nextIC;	// Instruction Counter
	
	reg interruptInhibitFlag;
	reg parityFaultInhibitFlag;
	reg overflowFaultInhibitFlag;
	reg parityErrorFlag;
	
	reg zeroFlag;
	reg negativeFlag;
	reg carryFlag;
	reg overflowFlag;
	
	reg updateZeroFlag;
	reg updateNegativeFlag;
	reg updateCarryFlag;
	reg updateOverflowFlag;
	reg [17:0] tempResultReg;
	reg [17:0] tempSecondResultReg;
	reg [3:0] resultCopyDest;
	
	parameter resultNoCopy 	= 4'b0000;
	parameter resultToA		= 4'b0001;
	parameter resultToQ		= 4'b0010;
	parameter resultToX1		= 4'b0011;
	parameter resultToX2		= 4'b0100;
	parameter resultToX3		= 4'b0101;
	parameter resultToAQ		= 4'b0111;
	parameter resultAddToA	= 4'b1001;
	parameter resultToX1C	= 4'b1010;
	parameter resultToX2C	= 4'b1011;
	parameter resultToX3C	= 4'b1100;
	
	reg [3:0] memoryReadPostOp;
	
	parameter MEMORY_LOAD			= 4'b0000;
	parameter MEMORY_ADD_TO_A		= 4'b0001;
	parameter MEMORY_ADD_TO_Q		= 4'b0010;
	parameter MEMORY_ADD_TO_AQ		= 4'b0011;
	parameter MEMORY_AND_TO_A		= 4'b0100;
	parameter MEMORY_OR_TO_A		= 4'b0101;
	parameter MEMORY_EOR_TO_A		= 4'b0111;
	parameter MEMORY_SUB_FROM_A	= 4'b1000;
	parameter MEMORY_SUB_FROM_Q	= 4'b1001;
	parameter MEMORY_SUB_FROM_AQ	= 4'b1010;
	
	assign indicators[0] = zeroFlag;
	assign indicators[1] = negativeFlag;
	assign indicators[2] = carryFlag;
	assign indicators[3] = overflowFlag;
	assign indicators[4] = interruptInhibitFlag;
	assign indicators[5] = parityFaultInhibitFlag;
	assign indicators[6] = overflowFaultInhibitFlag;
	assign indicators[7] = parityErrorFlag;
	
	// Other signals
	reg singleStep;
	reg lastSingleStep;
	reg haltFlag;
	
	wire [1:0] opCodeGroup;		// Indicates the op code group number based on the primary op code (0, 1 or 2)
	wire [5:0] opCode;			// Primary op code
	wire indirectFlag;			// When true indicates that indirect addressing is being done
	wire [1:0] tag;				// For group 0 op codes, contains the tag field
	wire [8:0] displacement;	// For group 0 and 1 op codes, contains the displacement field
	wire [2:0] s1;					// For group 1 and 2 op codes, contains the S1 field
	wire [2:0] s2;					// For group 2 op codes, contains the S2 field
	wire [5:0] k;					// For group 2 op codes, contains the K field
	
	reg [2:0] charAddressMode;	// Characater addressing mode when using an X register
	
	// Fetch cycle state
	parameter FETCH_SETUP	= 2'b00;
	parameter FETCH_DELAY	= 2'b01;
	parameter FETCH_READ		= 2'b10;
	
	// Address resolution states
	parameter RESOLVE_SETUP	= 2'b00;
	parameter RESOLVE_READ	= 2'b01;
	parameter RESOLVE_CALC	= 2'b10;
	
	// Memory cycle state
	parameter M_WRITE_DOUBLE_WORD		= 4'b0001;
	parameter M_FINISH_FIRST_WRITE	= 4'b0010;
	parameter M_START_SECOND_WRITE	= 4'b0011;
	parameter M_WRITE_WORD				= 4'b0100;
	parameter M_FINISH_WRITE			= 4'b0101;
	parameter M_READ_DOUBLE_WORD		= 4'b1000;
	parameter M_FINISH_FIRST_READ		= 4'b1001;
	parameter M_START_SECOND_READ		= 4'b1010;
	parameter M_READ_WORD				= 4'b1011;
	parameter M_FINISH_READ				= 4'b1100;
	parameter M_RMW_READ					= 4'b1101;
	parameter M_RMW_MODIFY				= 4'b1110;
	parameter M_RMW_WRITE				= 4'b1111;

	reg [2:0] readModWriteOp;
	
	parameter RMW_NOP			= 3'b001;
	parameter RMW_ADD_A		= 3'b001;
	parameter RMW_ADD_ONE	= 3'b010;
	parameter RMW_AND_A		= 3'b011;
	parameter RMW_OR_A		= 3'b100;
	parameter RMW_EOR_A		= 3'b101;
	
	// Set up the program module for reset processing
	reg [7:0] programAddress;
	wire [17:0] programData;
	wire programLoadComplete;
	
	parameter PROG_SETUP  = 2'b00;
	parameter PROG_WRITE  = 2'b01;
	parameter PROG_FINISH = 2'b10;
	parameter PROG_NEXT   = 2'b11;
	
	Datanet355Programmer _pgm0 (clk, reset, programAddress, programData, programLoadComplete);
	
	wire [17:0] romReadData;
	reg [7:0] romAddress;
	
	RomInitBinary #(.INIT_FILE("dn355_ROM.init")) rom0(clk, romAddress, romReadData);
	
	reg [2:0] camRow;
	reg [2:0] camCol;
	wire [3:0] camResult;
	
	CharacterAddMatrix _cam0(reset, camRow, camCol, camResult);
	
	InstructionDecoder instructionDecoder0(instruction, opCode, opCodeGroup, indirectFlag, tag, displacement, s1, s2, k);
	
	// Instruction cycle and reset processing
	
	always @ (posedge(clk), posedge(reset))
	begin
		if (reset)
			begin
				nextIC <= 0;
				regA <= 0;
				regQ <= 0;
				regX1 <= 0;
				regX2 <= 0;
				regX3 <= 0;
				zeroFlag <= 1;
				negativeFlag <= 0;
				carryFlag <= 0;
				overflowFlag <= 0;
				interruptInhibitFlag <= 0;
				parityFaultInhibitFlag <= 0;
				overflowFaultInhibitFlag <= 0;
				parityErrorFlag <= 0;
				regS <= 0;
				instruction <= 18'o777773;
				programAddress <= 0;
				romAddress <= 0;
				runIndicator <= 0;
				haltFlag <= 0;
				lastSingleStep <= 0;
				singleStep <= 0;
				currentIC <= 0;
				charAddressMode <= 0;
				camRow <= 0;
				camCol <= 0;
				memoryAddress <= 0;
				memoryWriteEnable <= 0;
				memoryChipSelect <= 0;
				cpuState <= CPU_PROG_LOAD;
				programState <= PROG_SETUP;
				fetchState <= FETCH_SETUP;
				memoryReadPostOp <= MEMORY_LOAD;
				diagnosticCode <= 0;
			end
		else 
			case (cpuState)
				CPU_FETCH:
				begin
					charAddressMode <= 0;

					case (fetchState)
						FETCH_SETUP:		// Set up memory acces to get instruction in program
						begin
							runIndicator <= 1;

							memoryAddress <= nextIC;
							memoryWriteEnable <= 0;
							memoryChipSelect <= 1;
							
							currentIC <= nextIC;  // Save address of PC for breakpoint
							fetchState <= FETCH_DELAY;
						end
						
						FETCH_DELAY:		// Wait for memory read to complete
							fetchState <= FETCH_READ;
							
						FETCH_READ:		// Get get instruction
						begin
							instruction <= memoryReadData;
							nextIC <= nextIC + 15'b1;
							cpuState <= CPU_DECODE;
							fetchState <= FETCH_SETUP;
						end
						
						default:	diagnosticCode <= 18'o1;
					endcase
				end
				
				CPU_DECODE:  	// Disassemble the instruction 
				begin
					memoryChipSelect <= 0;
					updateZeroFlag <= 0;
					updateNegativeFlag <= 0;
					updateCarryFlag <= 0;
					updateOverflowFlag <= 0;
					resultCopyDest <= resultNoCopy;
					memoryReadPostOp <= MEMORY_LOAD;
					readModWriteOp <= RMW_NOP;

					if (opCodeGroup == 0)
					begin
						case (tag)
							2'b00: effectiveAddress <= $signed(displacement) + currentIC;
							2'b01: 
							begin
								effectiveAddress <= $signed(displacement) + regX1[14:0];
								charAddressMode <= regX1[17:15];
							end
							2'b10: 
							begin
								effectiveAddress <= $signed(displacement) + regX2[14:0];
								charAddressMode <= regX2[17:15];
							end
							2'b11: 
							begin
								effectiveAddress <= $signed(displacement) + regX3[14:0];
								charAddressMode <= regX3[17:15];
							end
						endcase
						cpuState <= (indirectFlag) ? CPU_RESOLVE : CPU_EXECUTE;
						resolveState <= RESOLVE_SETUP;
					end
					else
						cpuState <= CPU_EXECUTE;
				end
				
				CPU_RESOLVE:		// Resolve the effective address for a memory access instruction
				begin
					case (resolveState)
						RESOLVE_SETUP:		// Set up to read the indirect word from memory
						begin
							memoryAddress <= effectiveAddress;
							memoryWriteEnable <= 0;
							memoryChipSelect <= 1;
							resolveState <= RESOLVE_READ;
						end
						
						RESOLVE_READ:		// Delay cycle for memory read
							resolveState <= RESOLVE_CALC;
							
						RESOLVE_CALC:		// Calculate next memory read or bail out
						begin
							memoryChipSelect <= 0;

							// Update the effective address based on the tag in the memory word
							case (memoryReadData[16:15])
								2'b00: effectiveAddress <= memoryReadData[14:0];
								2'b01: effectiveAddress <= memoryReadData[14:0] + regX1[14:0];
								2'b10: effectiveAddress <= memoryReadData[14:0] + regX2[14:0];
								2'b11: effectiveAddress <= memoryReadData[14:0] + regX3[14:0];
							endcase
							
							resolveState <= RESOLVE_SETUP;
							
							// If the indirect flag in the memory word is set we need another CPU_RESOLVE cycle
							cpuState <= (memoryReadData[17]) ? CPU_RESOLVE : CPU_EXECUTE;
						end

						default:	diagnosticCode <= 18'o1;
					
					endcase
				end
					
				CPU_EXECUTE: 	// Perform desired operation
				begin
					case (opCodeGroup)
						2'b00:		// Group 0 Op Codes: Memory Reference
						begin
							case (opCode)
								6'o00: 			// Temporary HALT instruction (not a DN355 op code)
								begin
									diagnosticCode <= displacement;
									haltFlag <= 1;												
									cpuState <= CPU_FINISH;
								end
								6'o03:			// LDX2
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToX2;
									cpuState <= CPU_MEMORY;
								end
								6'o04:			// LDAQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_DOUBLE_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								3'o6: 			// ADA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_ADD_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o07:			// LDA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o10:			// TSY
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= currentIC + 18'b1;
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_WORD;
									nextIC <= effectiveAddress + 15'b1;
									cpuState <= CPU_MEMORY;
								end
								6'o14:			// STAQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= regA;
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_DOUBLE_WORD;
									cpuState <= CPU_MEMORY;
								end
								6'o15: 			// ADAQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_DOUBLE_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_ADD_TO_AQ;
									resultCopyDest <= resultToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o16:			// ASA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_RMW_READ;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									readModWriteOp <= RMW_ADD_A;
									cpuState <= CPU_MEMORY;
								end
								6'o17:			// STA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= regA;
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_WORD;
									cpuState <= CPU_MEMORY;
								end
								6'o20:			// SZN
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									cpuState <= CPU_MEMORY;
								end
								6'o24: 			// SBAQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_DOUBLE_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_SUB_FROM_AQ;
									resultCopyDest <= resultToAQ;
									cpuState <= CPU_MEMORY;
								end
								6'o26: 			// SBA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_SUB_FROM_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o32:			// ANSA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_RMW_READ;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									readModWriteOp <= RMW_AND_A;
									cpuState <= CPU_MEMORY;
								end
								6'o34: 			// ANA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_AND_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o35: 			// ERA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_EOR_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o37: 			// ORA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_OR_TO_A;
									resultCopyDest <= resultToA;
									cpuState <= CPU_MEMORY;
								end
								6'o41:			// LDX3
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToX3;
									cpuState <= CPU_MEMORY;
								end
								6'o43:			// LDX1
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToX1;
									cpuState <= CPU_MEMORY;
								end
								6'o45:			// TNC
								begin
									if (~carryFlag) 
										nextIC <= effectiveAddress;		
									cpuState <= CPU_FINISH;
								end
								6'o46: 			// ADQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_ADD_TO_Q;
									resultCopyDest <= resultToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o47:			// LDQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									resultCopyDest <= resultToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o54:			// STI
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= {regS, 4'b0, indicators};
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_WORD;
									cpuState <= CPU_MEMORY;
								end
								6'o55: 			// TOV
								begin
									if (overflowFlag) 
										nextIC <= effectiveAddress;	
									cpuState <= CPU_FINISH;
								end
								6'o56:			// STZ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= 18'o0;
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_WORD;
									cpuState <= CPU_MEMORY;
								end
								6'o57:			// STQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteData <= regQ;
									memoryWriteEnable <= 1;
									memoryChipSelect <= 1;
									memoryState <= M_WRITE_WORD;
									cpuState <= CPU_MEMORY;
								end
								6'o62:			// ERSA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_RMW_READ;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									readModWriteOp <= RMW_EOR_A;
									cpuState <= CPU_MEMORY;
								end
								6'o64: 			// TNZ
								begin
									if (~zeroFlag) 
										nextIC <= effectiveAddress;		
									cpuState <= CPU_FINISH;
								end
								6'o65: 			// TPL
								begin
									if (~negativeFlag) 
										nextIC <= effectiveAddress;	
									cpuState <= CPU_FINISH;
								end
								6'o66: 			// SBQ
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_READ_WORD;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									memoryReadPostOp <= MEMORY_SUB_FROM_Q;
									resultCopyDest <= resultToQ;
									cpuState <= CPU_MEMORY;
								end
								6'o71: 			// TRA
								begin
									nextIC <= effectiveAddress;							
									cpuState <= CPU_FINISH;
								end
								6'o72:			// ORSA
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_RMW_READ;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									readModWriteOp <= RMW_OR_A;
									cpuState <= CPU_MEMORY;
								end
								6'o74: 			// TZE
								begin
									if (zeroFlag) 
										nextIC <= effectiveAddress; 		
									cpuState <= CPU_FINISH;
								end
								6'o75: 			// TMI
								begin
									if (negativeFlag) 
										nextIC <= effectiveAddress;	
									cpuState <= CPU_FINISH;
								end
								6'o76:			// AOS
								begin
									memoryAddress <= effectiveAddress;
									memoryWriteEnable <= 0;
									memoryChipSelect <= 1;
									memoryState <= M_RMW_READ;
									updateZeroFlag <= 1'b1;
									updateNegativeFlag <= 1'b1;
									readModWriteOp <= RMW_ADD_ONE;
									cpuState <= CPU_MEMORY;
								end
								default:	diagnosticCode <= 18'o16;
							endcase
						end
						2'b01:		// Group 1 Op Codes: Non-Memory Reference
						begin
							case (opCode)
								6'o22:
								begin
									case (s1)
										3'o0: 	// IANA
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											tempResultReg <= regA & $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o1: 	// IORA
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											tempResultReg <= regA | $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o3: 	// IERA
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											tempResultReg <= regA ^ $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o15;
									endcase
								end
								6'o73:
								begin
									case (s1)
										3'o0: 	// SEL
										begin
											regS <= displacement[5:0];	
											cpuState <= CPU_FINISH;
										end
										3'o1:		// IACX1
										begin
											updateZeroFlag <= 1'b1;
											tempResultReg <= $signed(regX1[14:0]) + $signed(displacement[5:0]);
											camRow <= regX1[17:15];
											camCol <= displacement[8:6];
											resultCopyDest <= resultToX1C;
											cpuState <= CPU_FINISH;
										end
										3'o2:		// IACX2
										begin
											updateZeroFlag <= 1'b1;
											tempResultReg <= $signed(regX2[14:0]) + $signed(displacement[5:0]);
											camRow <= regX2[17:15];
											camCol <= displacement[8:6];
											resultCopyDest <= resultToX2C;
											cpuState <= CPU_FINISH;
										end
										3'o3:		// IACX3
										begin
											updateZeroFlag <= 1'b1;
											tempResultReg <= $signed(regX3[14:0]) + $signed(displacement[5:0]);
											camRow <= regX3[17:15];
											camCol <= displacement[8:6];
											resultCopyDest <= resultToX3C;
											cpuState <= CPU_FINISH;
										end
										3'o4:		// ILQ 
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											tempResultReg <= $signed(displacement);
											resultCopyDest <= resultToQ;
											cpuState <= CPU_FINISH;
										end
										3'o5:		// IAQ 
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											{carryFlag,tempResultReg} <= regQ + $signed(displacement);
											resultCopyDest <= resultToQ;
											cpuState <= CPU_FINISH;
										end
										3'o6: 	// ILA
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											tempResultReg <= $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										3'o7: 	// IAA
										begin
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											{carryFlag,tempResultReg} <= regA + $signed(displacement);
											resultCopyDest <= resultToA;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o14;
									endcase
								end
								default:	diagnosticCode <= 18'o13;
							endcase
						end
						2'b10:		// Group 2 Op Codes: Register Manipulation
						begin
							case (s1)
								3'o0:
								begin
									case (s2)
										3'o2:		// CAX2 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX2;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										
										default:	diagnosticCode <= 18'o2;
									endcase
								end
								3'o2:
								begin
									case (s2)
										3'o2:		// CX1A 
										begin
											tempResultReg <= regX1;
											resultCopyDest <= resultToA;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o3;
									endcase
								end
								3'o3:
								begin
									case (s2)
										3'o2:		// CX2A 
										begin
											tempResultReg <= regX2;
											resultCopyDest <= resultToA;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										3'o3:		// CX3A 
										begin
											tempResultReg <= regX3;
											resultCopyDest <= resultToA;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o4;
									endcase
								end
								3'o4:
								begin
									case (s2)
										3'o2:		// CAX1 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX1;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										3'o3:		// CAX3 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToX3;
											updateZeroFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o5;
									endcase
								end
								3'o6:
								begin
									case (s2)
										3'o3:		// CAQ 
										begin
											tempResultReg <= regA;
											resultCopyDest <= resultToQ;
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o6;
									endcase
								end
								3'o7:
								begin
									case (s2)
										3'o3:		// CQA 
										begin
											tempResultReg <= regQ;
											resultCopyDest <= resultToA;
											updateZeroFlag <= 1'b1;
											updateNegativeFlag <= 1'b1;
											cpuState <= CPU_FINISH;
										end
										default:	diagnosticCode <= 18'o7;
									endcase
								end
								default:	diagnosticCode <= 18'o10;
							endcase
						end
						default:	diagnosticCode <= 18'o11;
					endcase
				end
				
				CPU_MEMORY:		// Perform a memory cycle
				begin
					case (memoryState)
						M_WRITE_DOUBLE_WORD:
						begin
							memoryState <= M_FINISH_FIRST_WRITE;
						end
						M_FINISH_FIRST_WRITE:
						begin
							memoryWriteEnable <= 1'b0;
							memoryChipSelect <= 1'b0;
							memoryState <= M_START_SECOND_WRITE;
						end
						M_START_SECOND_WRITE:
						begin
							memoryAddress <= memoryAddress + 15'b1;
							memoryWriteData <= regQ;
							memoryWriteEnable <= 1'b1;
							memoryChipSelect <= 1'b1;
							memoryState <= M_WRITE_WORD;
						end
						M_WRITE_WORD:
						begin
							memoryState <= M_FINISH_WRITE;
						end
						M_FINISH_WRITE:
						begin
							memoryWriteEnable <= 1'b0;
							memoryChipSelect <= 1'b0;
							cpuState <= CPU_FINISH;
						end
						M_READ_DOUBLE_WORD:
						begin
							memoryState <= M_FINISH_FIRST_READ;
						end
						M_FINISH_FIRST_READ:
						begin
							tempSecondResultReg <= memoryReadData;
							memoryChipSelect <= 1'b0;
							memoryState <= M_START_SECOND_READ;
						end
						M_START_SECOND_READ:
						begin
							memoryAddress <= memoryAddress + 15'b1;
							memoryChipSelect <= 1'b1;
							memoryState <= M_READ_WORD;
						end
						M_READ_WORD:
						begin
							memoryState <= M_FINISH_READ;
						end
						M_FINISH_READ:
						begin
							case (memoryReadPostOp)
								MEMORY_LOAD:			tempResultReg <= memoryReadData;
								MEMORY_ADD_TO_A:		{carryFlag,tempResultReg} <= regA + memoryReadData;
								MEMORY_ADD_TO_Q:		{carryFlag,tempResultReg} <= regQ + memoryReadData;
								MEMORY_ADD_TO_AQ:		{carryFlag,tempSecondResultReg,tempResultReg} <= {regA,regQ} + {tempSecondResultReg,memoryReadData};
								MEMORY_AND_TO_A:		tempResultReg <= regA & memoryReadData;
								MEMORY_OR_TO_A:		tempResultReg <= regA | memoryReadData;
								MEMORY_EOR_TO_A:		tempResultReg <= regA ^ memoryReadData;
								MEMORY_SUB_FROM_A:	{carryFlag,tempResultReg} <= regA - memoryReadData;
								MEMORY_SUB_FROM_Q:	{carryFlag,tempResultReg} <= regQ - memoryReadData;
								MEMORY_SUB_FROM_AQ:	{carryFlag,tempSecondResultReg,tempResultReg} <= {regA,regQ} - {tempSecondResultReg,memoryReadData};
								default: 				
								begin
									diagnosticCode <= 18'o22;
									cpuState <= CPU_HALT;
								end
							endcase
							memoryChipSelect <= 1'b0;
							cpuState <= CPU_FINISH;
						end
						M_RMW_READ:
						begin
							memoryState <= M_RMW_MODIFY;
						end
						M_RMW_MODIFY:
						begin
							case (readModWriteOp)
								RMW_ADD_A:
								begin
									{carryFlag, memoryWriteData} <= memoryReadData + regA;
								end
								RMW_ADD_ONE:
								begin
									{carryFlag, memoryWriteData} <= memoryReadData + 18'b1;
									memoryWriteEnable <= 1'b1;
								end
								RMW_AND_A:
								begin
									memoryWriteData <= memoryReadData & regA;
								end
								RMW_OR_A:
								begin
									memoryWriteData <= memoryReadData | regA;
								end
								RMW_EOR_A:
								begin
									memoryWriteData <= memoryReadData ^ regA;
								end
								default: 
								begin
									diagnosticCode <= 18'o21;
									cpuState <= CPU_HALT;
								end
							endcase
							memoryWriteEnable <= 1'b1;
							memoryState <= M_RMW_WRITE;
						end
						M_RMW_WRITE:
						begin
							memoryState <= M_FINISH_WRITE;
						end
						default: diagnosticCode <= 18'o20;
					endcase
				end
				
				CPU_FINISH:		// Finish processing current instruction
				begin
					case (resultCopyDest)
						resultToA:	regA <= tempResultReg;
						resultToQ:	regQ <= tempResultReg;
						resultToX1:	regX1 <= tempResultReg;
						resultToX2:	regX2 <= tempResultReg;
						resultToX3:	regX3 <= tempResultReg;
						resultToAQ:
						begin
							regA <= tempSecondResultReg;
							regQ <= tempResultReg;
						end
						resultToX1C: regX1 <= (camResult & 4'o10) ? ({camResult[2:0],($signed(tempResultReg[14:0])+1)}) 
																				: ({camResult[2:0],tempResultReg[14:0]});
						resultToX2C: regX2 <= (camResult & 4'o10) ? ({camResult[2:0],($signed(tempResultReg[14:0])+1)}) 
																				: ({camResult[2:0],tempResultReg[14:0]});
						resultToX3C: regX3 <= (camResult & 4'o10) ? ({camResult[2:0],($signed(tempResultReg[14:0])+1)}) 
																				: ({camResult[2:0],tempResultReg[14:0]});
					endcase
					if (resultCopyDest == resultToAQ)
					begin
						zeroFlag <= (updateZeroFlag) ? ((tempResultReg | tempSecondResultReg) ? 1'b0 : 1'b1) : zeroFlag;
					end
					else
					begin
						zeroFlag <= (updateZeroFlag) ? ((tempResultReg) ? 1'b0 : 1'b1) : zeroFlag;
					end
						negativeFlag <= (updateNegativeFlag) ? (tempResultReg[17] ? 1'b1 : 1'b0) : negativeFlag;
					singleStep <= 1'b0;
					cpuState <= (haltFlag) ? CPU_HALT : (run) ? CPU_FETCH : CPU_HALT;
				end
				
				CPU_PROG_LOAD:		// Load initial program into RAM from either PGM or ROM
				begin
					if (programRequest)
					begin
						case (programState)
							PROG_SETUP:	// Set up for write cycle
							begin
								memoryAddress <= programAddress;
								memoryWriteData <= programData;
								programState <= PROG_WRITE;
							end
							PROG_WRITE:	// Perform write cycle for program data
							begin
								memoryWriteEnable <= 1'b1;
								memoryChipSelect <= 1'b1;
								programState <= PROG_FINISH;
							end
							PROG_FINISH:	// Complete the write cycle
							begin
								memoryWriteEnable <= 1'b0;
								memoryChipSelect <= 1'b0;
								programState <= PROG_NEXT;
							end
							PROG_NEXT:		// Set up for next write cycle
							begin
								if (programLoadComplete)
								begin
									cpuState <= (run) ? CPU_FETCH : CPU_HALT;
								end
								else
								begin
									programAddress <= programAddress + 8'b1;
									programState <= PROG_SETUP;
								end
							end
						endcase
					end
					else
					begin
						case (programState)
							PROG_SETUP:	// Set up for write cycle
							begin
								memoryAddress <= romAddress;
								memoryWriteData <= romReadData;
								programState <= PROG_WRITE;
							end
							PROG_WRITE:	// Perform write cycle for program data
							begin
								memoryWriteEnable <= 1'b1;
								memoryChipSelect <= 1'b1;
								programState <= PROG_FINISH;
							end
							PROG_FINISH:	// Complete the write cycle
							begin
								memoryWriteEnable <= 1'b0;
								memoryChipSelect <= 1'b0;
								programState <= PROG_NEXT;
							end
							PROG_NEXT:		// Set up for next write cycle
							begin
								if (romAddress == 255)
								begin
									cpuState <= (run) ? CPU_FETCH : CPU_HALT;
								end
								else
								begin
									romAddress <= romAddress + 8'b1;
									programState <= PROG_SETUP;
								end
							end
						endcase
					end
				end
				
				
				CPU_HALT:			// Processor is halted
				begin
					runIndicator <= 0;
					if (singleStepRequest && !lastSingleStep)
					begin
						singleStep <= 1'b1;
						lastSingleStep <= 1'b1;
					end
					else if (!singleStepRequest && lastSingleStep)
					begin
						singleStep <= 1'b0;
						lastSingleStep <= 1'b0;
					end
					haltFlag <= haltFlag && run;
					cpuState <= (haltFlag) ? CPU_HALT : (run | singleStep) ? CPU_FETCH : CPU_HALT;
				end
			endcase
	end
endmodule
