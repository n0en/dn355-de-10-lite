// PGM: This module is used to define a program for the DN355 that is loaded on reset

`define DATA asmData(

`define DIR 1'o0,
`define IND 1'o1,

`define tIC 2'o0,
`define tX1 2'o1,
`define tX2 2'o2,
`define tX3 2'o3,

`define MPF asmG0(6'o01,
`define ADCX2 asmG0(6'o02,
`define LDX2 asmG0(6'o03,
`define LDAQ asmG0(6'o04,
`define ADA asmG0(6'o06,
`define LDA asmG0(6'o07,
`define TSY asmG0(6'o10,
`define STX2 asmG0(6'o13,
`define STAQ asmG0(6'o14,
`define ADAQ asmG0(6'o15,
`define ASA asmG0(6'o16,
`define STA asmG0(6'o17,
`define SZN asmG0(6'o20,
`define DVF asmG0(6'o21,
`define CMPX2 asmG0(6'o23,
`define SBAQ asmG0(6'o24,
`define SBA asmG0(6'o26,
`define CMPA asmG0(6'o27,
`define LDEX asmG0(6'o30,
`define CANA asmG0(6'o31,
`define ANSA asmG0(6'o32,
`define ANA asmG0(6'o34,
`define ERA asmG0(6'o35,
`define SSA asmG0(6'o36,
`define ORA asmG0(6'o37,
`define ADCX3 asmG0(6'o40,
`define LDX3 asmG0(6'o41,
`define ADCX1 asmG0(6'o42,
`define LDX1 asmG0(6'o43,
`define LDI asmG0(6'o44,
`define TNC asmG0(6'o45,
`define ADQ asmG0(6'o46,
`define LDQ asmG0(6'o47,
`define STX3 asmG0(6'o50,
`define STX1 asmG0(6'o53,
`define STI asmG0(6'o54,
`define TOV asmG0(6'o55,
`define STZ asmG0(6'o56,
`define STQ asmG0(6'o57,
`define CIOC asmG0(6'o60,
`define CMPX3 asmG0(6'o61,
`define ERSA asmG0(6'o62,
`define CMPX1 asmG0(6'o63,
`define TNZ asmG0(6'o64,
`define TPL asmG0(6'o65,
`define SBQ asmG0(6'o66,
`define CMPQ asmG0(6'o67,
`define STEX asmG0(6'o70,
`define TRA asmG0(6'o71,
`define ORSA asmG0(6'o72,
`define TZE asmG0(6'o74,
`define TMI asmG0(6'o75,
`define AOS asmG0(6'o76,

`define RIER asmG1(3'o0,6'o12,9'o0
`define RIA asmG1(3'o4,6'o12,9'o0
`define IANA asmG1(3'o0,6'o22,
`define IORA asmG1(3'o1,6'o22,
`define ICANA asmG1(3'o2,6'o22,
`define IERA asmG1(3'o3,6'o22,
`define ICMPA asmG1(3'o4,6'o22,
`define SIER asmG1(3'o0,6'o52,9'o0
`define SIC asmG1(3'o4,6'o52,
`define SEL asmG1(3'o0,6'o73,
`define IACX1 asmG1(3'o1,6'o73,
`define IACX2 asmG1(3'o2,6'o73,
`define IACX3 asmG1(3'o3,6'o73,
`define ILQ asmG1(3'o4,6'o73,
`define IAQ asmG1(3'o5,6'o73,
`define ILA asmG1(3'o6,6'o73,
`define IAA asmG1(3'o7,6'o73,

`define CAX2 asmG2(3'o0,6'o33,3'o2,0
`define LLS  asmG2(3'o0,6'o33,3'o4,
`define LRS  asmG2(3'o0,6'o33,3'o5,
`define ALS  asmG2(3'o0,6'o33,3'o6,
`define ARS  asmG2(3'o0,6'o33,3'o7,
`define NRML asmG2(3'o1,6'o33,3'o4,
`define NRM  asmG2(3'o1,6'o33,3'o6,
`define NOP  asmG2(3'o2,6'o33,3'o1,
`define CX1A asmG2(3'o2,6'o33,3'o2,
`define LLR  asmG2(3'o2,6'o33,3'o4,
`define LRL  asmG2(3'o2,6'o33,3'o5,
`define ALR  asmG2(3'o2,6'o33,3'o6,
`define ARL  asmG2(3'o2,6'o33,3'o7,
`define INH  asmG2(3'o3,6'o33,3'o1,0
`define CX2A asmG2(3'o3,6'o33,3'o2,
`define CX3A asmG2(3'o3,6'o33,3'o3,
`define ALP  asmG2(3'o3,6'o33,3'o6,
`define DIS  asmG2(3'o4,6'o33,3'o1,0
`define CAX1 asmG2(3'o4,6'o33,3'o2,0
`define CAX3 asmG2(3'o4,6'o33,3'o3,0
`define QLS  asmG2(3'o4,6'o33,3'o6,
`define QRS  asmG2(3'o4,6'o33,3'o7,
`define CAQ  asmG2(3'o6,6'o33,3'o3,0
`define QLR  asmG2(3'o6,6'o33,3'o6,
`define QRL  asmG2(3'o6,6'o33,3'o7,
`define ENI  asmG2(3'o7,6'o33,3'o1,0
`define CQA  asmG2(3'o7,6'o33,3'o3,0
`define QLP  asmG2(3'o7,6'o33,3'o6,


`define _ );

module Datanet355Programmer (
	input wire clk, 
	input wire reset,
	input wire [7:0] address,
	output reg [17:0] data,
	output reg loadComplete
);

	integer IP;
	reg [17:0] progMem [0:255];
	
	task asmData ();
		input [17:0] DATA;
		progMem[IP] <= DATA;
		IP = IP + 1;
	endtask
	
	task asmG0 ();
		input [5:0] OP;
		input I;
		input [1:0] T;
		input [8:0] D;
		progMem[IP]	<= (I << 17) | (T << 15) | (OP << 9) | D;
		IP = IP + 1;
	endtask
	
	task asmG1 ();
		input [2:0] S1;
		input [5:0] OP;
		input [8:0] Y;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | Y;
		IP = IP + 1;
	endtask

	task asmG2 ();
		input [2:0] S1;
		input [5:0] OP;
		input [2:0] S2;
		input [5:0] K;
		progMem[IP]	<= (S1 << 15) | (OP << 9) | (S2 << 6) | K;
		IP = IP + 1;
	endtask

	always @ (address, reset)
	begin
		if (reset)
		begin
			loadComplete <= 1'b0;
			IP = 0;
			`LDA	`DIR `tIC 9		`_
			`IAA	96		`_
			`CAQ			`_
			`IAQ	7		`_
			`TSY	`DIR `tIC 5 `_
			`ILA	0		`_
			`IAA	1		`_
			`CAX1			`_
			`TRA	`DIR `tIC 9'o770 `_
			`DATA	18'o101010	`_
			`ILA	8		`_
			`TRA	`IND `tIC 9'o776 `_
		end
		else
		begin
			data <= progMem[address];
			loadComplete <= ((address + 1) >= IP) ? 1'b1 : 1'b0;
		end
	end

endmodule
