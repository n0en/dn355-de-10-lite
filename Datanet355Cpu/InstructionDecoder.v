// InstructionDecoder: Break up the current instruction based on the op code
module InstructionDecoder(
	input wire [17:0] instruction,
	output wire [5:0] opCode,
	output wire [1:0] opCodeGroup,	// Indicates the op code group number based on the primary op code (0, 1 or 2)
	output wire indirectFlag,			// When true indicates that indirect addressing is being done
	output wire [1:0] tag,				// For group 0 op codes, contains the tag field
	output wire [8:0] displacement,	// For group 0 and 1 op codes, contains the displacement field
	output wire [2:0] s1,				// For group 1 and 2 op codes, contains the S1 field
	output wire [2:0] s2,				// For group 2 op codes, contains the S2 field
	output wire [5:0] k					// For group 2 op codes, contains the K field
);

	parameter GROUP_0 = 2'b00;
	parameter GROUP_1 = 2'b01;
	parameter GROUP_2 = 2'b10;

	assign opCode = instruction[14:9];
	
	GroupDecoder groupDecoder1(opCode, opCodeGroup);

	assign indirectFlag 		= instruction[17] && (opCodeGroup == GROUP_0);
	assign tag 					= (opCodeGroup == GROUP_0) ? instruction[16:15] : 2'b00;
	assign displacement 		= (opCodeGroup != GROUP_2) ? instruction[8:0] : 9'b0;
	assign s1 					= ((opCodeGroup == GROUP_1) || (opCodeGroup == GROUP_2)) ? instruction[17:15] : 3'b0;
	assign s2 					= (opCodeGroup == GROUP_2) ? instruction[8:6] : 3'b0;
	assign k 					= (opCodeGroup == GROUP_1) ? instruction[5:0] : 3'b0;

endmodule

// GroupDecoder: Determine the operation code group based on the primary op code.
module GroupDecoder(
	input [5:0] primaryOpCode,
	output reg [1:0] groupNumber
);

always @ (primaryOpCode)
	case (primaryOpCode)
		6'o33: groupNumber <= 2'b10;
		6'o12: groupNumber <= 2'b01;
		6'o22: groupNumber <= 2'b01;
		6'o52: groupNumber <= 2'b01;
		6'o73: groupNumber <= 2'b01;
		default: groupNumber <= 2'b00;
	endcase

endmodule
