# DN355 DE-10 Lite

This is an attempt at creating a DN355/DN6600 using a DE-10 Lite FPGA development board.

## DE-10 Lite Switches, Keys and Indicators

The DE-10 Lite has two push button keys, ten two-position slide switches, ten red LEDs and six 7-segment displays.

The switches are mapped as follows (down is switch towards edge of board):

Switch 0     : Operation Select    : Run (up) / Halt (down)

Switch 1     : Program Load Source : PGM (up) / ROM (down)

Switch 2 - 4 : Unused

Switch 5 - 8 : Display Mode (octal value on the six 7-segment displays):

| SW Number ->         | 8 | 7 | 6 | 5 |
| -------------------- | - | - | - | - |
| Instruction Register | D | D | D | D |
| A Register           | D | D | D | U |
| Q Register           | D | D | U | D |
| X1 Register          | D | D | U | U |
| X2 Register          | D | U | D | D |
| X3 Register          | D | U | D | U |
| Instruction Counter  | D | U | U | U |
| Diagnostic Code      | U | D | D | D |
| Effective Address    | U | D | D | U |
| CPU State            | U | D | U | D |
| Indicators & S Reg   | U | D | U | U |

Switch 9 : CPU Clock Speed : 1 Hz (up) / 50 MHz (down)

Pushbutton KEY0 : Reset (when pressed, causes CPU reset and program reload from Program Load Source)

Pushbutton KEY1 : Single Step (note that in 1 Hz clock mode, you must hold it until LED 9 transitions from off to on for it to be detected)

LED indicators:

LED [4 - 0] : Current memory address
LED [7 - 5] : Current CPU state
LED [8]     : CPU running indicator
LED [9]     : 1 Hz clock

7-Segment Display Decimal Points:

| Digit | Indicator           |
| ----- | ---------           |
|   0   | Memory Chip Select  |
|   1   | Memory Write Enable |
|   2   | Overflow Flag       |
|   3   | Zero Flag           |
|   4   | Negative Flag       |
|   5   | Carry Flag          |



## Authors
Created by Dean S. Anderson with contributions by Charles Anthony.

## License
BSD 3-Clause License

## Project status
Currently in active development. A partial implementation of the DN355 CPU on a DE-10 Lite board is currently commited. It implements a large number of the machine instructions (though not all of them work correctly yet).
Next steps:
- Complete coding of the DN355 CPU
- Develop a Test and Diagnostic program to verify correct operation of the CPU
- Add a J1 processor core to allow using FORTH for development of the DN355 IOM and external interfaces
- Add a UART for the J1 processor to use during development
