// This module will generate a signal that is 1/4 of the input signal
module ClockDivideBy4(
	input clockIn,				// Input clock signal
	output reg clockOut		// Output of signal at 1/4 the input signal
);

	reg [1:0] counter = 0;
	
	always @ (posedge(clockIn))
	begin
		counter <= counter + 2'b1;
		clockOut <= (counter < 2) ? 1'b0 : 1'b1;
	end
	
endmodule
