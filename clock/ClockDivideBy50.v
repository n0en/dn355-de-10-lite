// This module will generate a signal that is 1/50 of the input signal
module ClockDivideBy50(
	input clockIn,				// Input clock signal
	output reg clockOut		// Output of signal at 1/50 the input signal
);

	reg [4:0] counter = 0;
	
	always @ (posedge(clockIn))
	begin
		counter <= counter + 2'b1;
		clockOut <= (counter < 25) ? 1'b0 : 1'b1;
	end
	
endmodule
