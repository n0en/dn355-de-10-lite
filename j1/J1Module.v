module J1Module (
	input wire CLK,
	output wire DUO_LED,
	input  wire DUO_SW1,
	input  wire RXD,
	output wire TXD,
	input  wire DTR
);

	localparam MHZ = 50;

	wire fclk;
  
	assign fclk = CLK;

	reg [25:0] counter;
	always @(posedge fclk)
		counter <= counter + 26'd1;
	assign DUO_LED = counter[25];

	// ------------------------------------------------------------------------

	wire uart0_valid, uart0_busy;
	wire [7:0] uart0_data;
	wire uart0_rd, uart0_wr;
	reg [31:0] baud = 32'd115200;
	wire UART0_RX;
	BidirectionalUart #(.CLKFREQ(MHZ * 1000000)) _uart0 (
		.clk(fclk),
		.resetq(1'b1),
		.baud(baud),
		.rx(RXD),
		.tx(TXD),
		.rd(uart0_rd),
		.wr(uart0_wr),
		.valid(uart0_valid),
		.busy(uart0_busy),
		.tx_data(dout_[7:0]),
		.rx_data(uart0_data));

	wire [15:0] mem_addr;
	wire [31:0] mem_din;
	wire mem_wr;
	wire [31:0] dout;

	wire [12:0] code_addr;
	wire [15:0] insn;

	wire io_wr;

	wire resetq = DTR;

	J1Cpu _j1 (
		.clk(fclk),
		.resetq(resetq),

		.io_wr(io_wr),
		.mem_addr(mem_addr),
		.mem_wr(mem_wr),
		.mem_din(mem_din),
		.dout(dout),
		.io_din({16'd0, uart0_data, 4'd0, DTR, uart0_valid, uart0_busy, DUO_SW1}),

		.code_addr(code_addr),
		.insn(insn)
	);

	J1Ram j1ram(
		.clk(fclk),
      .a_addr(mem_addr),
      .a_q(mem_din),
      .a_wr(mem_wr),
      .a_d(dout),
      .b_addr(code_addr),
      .b_q(insn));

	reg io_wr_;
	reg [15:0] mem_addr_;
	reg [31:0] dout_;
	always @(posedge fclk)
		{io_wr_, mem_addr_, dout_} <= {io_wr, mem_addr, dout};

	assign uart0_wr = io_wr_ & (mem_addr_ == 16'h0000);
	assign uart0_rd = io_wr_ & (mem_addr_ == 16'h0002);

endmodule
