module SingleAddressSeparateReadWriteRam
	(
		input									clock,
		input									ramEnable,
		input									writeEnable,
		input [HIGH_ADDRESS_BIT:0]		address,
		input	[HIGH_DATA_BIT:0]			inputData,
		output reg [HIGH_DATA_BIT:0]	outputData
	);
	
	parameter DATA_WIDTH = 18 ;
	parameter HIGH_DATA_BIT = DATA_WIDTH - 1;
	parameter ADDR_WIDTH = 15 ;
	parameter HIGH_ADDRESS_BIT = ADDR_WIDTH - 1;
	parameter RAM_DEPTH = 1 << ADDR_WIDTH;
	parameter MAX_ADDRESS = RAM_DEPTH - 1;

	reg [HIGH_DATA_BIT:0] ramMain [0:MAX_ADDRESS];
	
	always @ (posedge(clock))
		if (ramEnable)
		begin
			if (writeEnable)
				ramMain[address] <= inputData;
			outputData <= ramMain[address];
		end
		
endmodule
