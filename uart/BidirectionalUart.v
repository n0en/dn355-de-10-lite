// Implement a Rx/Tx (bidirectional) serial UART

module BidirectionalUart(
	input wire clk,
   input wire resetq,
   input wire [31:0] baud,
   input wire rx,           	// recv wire
   output wire tx,          	// xmit wire
   input wire rd,           	// read strobe
   input wire wr,           	// write strobe
   output wire valid,       	// has recv data 
   output wire busy,        	// is transmitting
   input wire [7:0] tx_data,
   output wire [7:0] rx_data 	// data
);

  parameter CLKFREQ = 1000000;

  RxUart #(.CLKFREQ(CLKFREQ)) _rx (
     .clk(clk),
     .resetq(resetq),
     .baud(baud),
     .uart_rx(rx),
     .rd(rd),
     .valid(valid),
     .data(rx_data));
	  
  TxUart #(.CLKFREQ(CLKFREQ)) _tx (
     .clk(clk),
     .resetq(resetq),
     .baud(baud),
     .uart_busy(busy),
     .uart_tx(tx),
     .uart_wr_i(wr),
     .uart_dat_i(tx_data));
	  
endmodule