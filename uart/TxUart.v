/*

Implement the transmit side of a serial UART.


-----+     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+----
     |     |     |     |     |     |     |     |     |     |     |     |
     |start|  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |stop1|stop2|
     |     |     |     |     |     |     |     |     |     |     |  ?  |
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+           +

*/

module TxUart(
	input wire clk,         		// System clock
   input wire resetq,

   // Outputs
   output wire uart_busy,   		// High means UART is transmitting
   output reg uart_tx,     		// UART transmit wire
	
   // Inputs
   input wire [31:0] baud,
   input wire uart_wr_i,   		// Raise to transmit byte
   input wire [7:0] uart_dat_i  	// 8-bit data
);

	parameter CLKFREQ = 1000000;

	reg [3:0] bitcount;
	reg [8:0] shifter;

	assign uart_busy = |bitcount;
	wire sending = |bitcount;

	wire ser_clk;

	wire starting = uart_wr_i & ~uart_busy;
	BaudRateGenerator #(.CLKFREQ(CLKFREQ)) _baudgen(
		.clk(clk),
		.resetq(resetq),
		.baud(baud),
		.restart(1'b0),
		.ser_clk(ser_clk));

	always @(negedge resetq or posedge clk)
	begin
		if (!resetq) begin
			uart_tx <= 1;
			bitcount <= 0;
			shifter <= 0;
		end else begin
			if (starting) begin
				shifter <= { uart_dat_i[7:0], 1'b0 };
				bitcount <= 1 + 8 + 1;		// 1 start bit, 8 data bits, 1 stop bit
			end

			if (sending & ser_clk) begin
				{ shifter, uart_tx } <= { 1'b1, shifter };
				bitcount <= bitcount - 4'd1;
			end
		end
  end

endmodule
